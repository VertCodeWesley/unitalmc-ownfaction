package me.DGCWesley.Faction;

import org.bukkit.plugin.java.JavaPlugin;

import me.DGCWesley.Faction.commands.Faction;
import me.DGCWesley.Faction.libary.functions;

public class main extends JavaPlugin {
	
	public void onEnable() {
		functions.load();
		onCommand();
	}
	
	public void onDisable() {
		functions.save();
	}
	
	public void onCommand() {
		getCommand("faction").setExecutor(new Faction(this));
	}

}
