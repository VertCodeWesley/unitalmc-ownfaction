package me.DGCWesley.Faction.libary;

import java.io.File;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import me.DGCWesley.Faction.records.records;
import me.DGCWesley.Faction.records.records.factionMemberRec;
import me.DGCWesley.Faction.records.records.factionRec;
import me.DGCWesley.Faction.records.records.playerRec;

public class functions {
	
	static File factionDirectory = files.factionDirectory;
	static File factionFile = files.factionFile;
	static File playerFile = files.playerFile;
	
	static FileConfiguration playerConfig = files.playerConfig;
	static FileConfiguration factionConfig = files.factionConfig; 
	
	static ArrayList<factionRec> factionsList = records.factionlist;
	static ArrayList<playerRec> playerList = records.playerlist;
	
	public static void save() {
		saveFactionFile();
		savePlayerFile();
	}
	
	public static void load() {
		if (factionDirectory.exists()) {
		loadFactionFile();
		loadFactionMembers();
		loadPlayerFile();
		}
	}
	
	private static void saveFactionFile() {
		if (factionsList.size() >= 0) {
			
		files.directoryIfNotExist(factionDirectory);
		files.fileIfNotExist(factionFile);
		
		try {
			factionConfig.load(factionFile);
			for (int a = 0; a < factionsList.size(); a++) {
				factionRec faction = factionsList.get(a);
				factionConfig.set("faction."+ a + ".name", faction.name);
				factionConfig.set("faction." + a + ".owner", faction.owner);
				factionConfig.set("faction." + a + ".mod", faction.mod);
				
				factionConfig.set("faction." + a + ".fhome.world", faction.fhome.getWorld().getName());
				factionConfig.set("faction." + a + ".fhome.x", faction.fhome.getBlockX());
				factionConfig.set("faction." + a + ".fhome.y", faction.fhome.getBlockY());
				factionConfig.set("faction." + a + ".fhome.z", faction.fhome.getBlockZ());
				factionConfig.set("faction." + a + ".fhome.yaw", faction.fhome.getYaw());
				factionConfig.set("faction." + a + ".fhome.pitch", faction.fhome.getPitch());
				for (int b = 0; b < faction.members.size(); b++) {
					factionMemberRec factionMember = faction.members.get(b);
					factionConfig.set("faction." + a + ".member." + b + ".name", factionMember.player.name);
					factionConfig.set("faction." + a + ".member." + b + ".uuid", factionMember.player.uuid);
					factionConfig.set("faction." + a + ".member." + b + ".rank", factionMember.rank);
				}
				factionConfig.save(factionFile);
			}
		}catch (Exception ex) { 
			ex.printStackTrace();
		}
		}
	}
	
	private static void savePlayerFile() {
		if (playerList.size() >= 0) {
		files.directoryIfNotExist(factionDirectory);
		files.fileIfNotExist(playerFile);
		try {
			playerConfig.load(playerFile);
			for (int a = 0; a < playerList.size(); a++) {
				playerRec player = playerList.get(a);
				playerConfig.set("player." + a + ".name", player.name);
				playerConfig.set("player." + a + ".uuid", player.uuid);
				playerConfig.set("player." + a + ".faction", player.faction.name);
				playerConfig.save(playerFile);
			}
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		}
	}
	
	private static void loadFactionFile() {
		if (factionFile.exists()) {
			try {
				factionConfig.load(factionFile);
				for (int a = 0; factionConfig.contains("faction." + a); a++) {
					factionRec factionRecord = new factionRec();
					factionsList.add(factionRecord);
					
					factionRecord.name = factionConfig.getString("faction." + a + ".name");
					
					World w = Bukkit.getWorld(factionConfig.getString("faction." + a + ".fhome.world"));
					
					double x = factionConfig.getInt("faction." + a + ".fhome.x");
					double y = factionConfig.getInt("faction." + a + ".fhome.y");
					double z = factionConfig.getInt("faction." + a + ".fhome.z");
					double yaw = factionConfig.getInt("faction." + a + ".fhome.yaw");
					double pitch = factionConfig.getInt("faction." + a + ".fhome.pitch");
					
					Location loc = new Location(w, x, y, z, (float) pitch, (float) yaw);
					
					factionRecord.fhome = loc;
					
					
				}
			}catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	private static void loadFactionMembers() {
		//TODO members + owner + mod
	}
	
	private static void loadPlayerFile() {
	}
	
	public static void createFaction(String name, Player p) {
		factionRec factionRecord = new factionRec();
		factionsList.add(factionRecord);
		
		factionRecord.name = name;
		factionRecord.owner = p.getName();
		factionRecord.mod = null;
		
		Location loc = new Location(p.getWorld(), 0, 0, 0, (float) 0, (float) 0);
		
		factionRecord.fhome = loc;
		
	}

}
