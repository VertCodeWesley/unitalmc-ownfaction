package me.DGCWesley.Faction.libary;

import java.io.File;
import java.util.ArrayList;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.DGCWesley.Faction.records.records;

public class files {
	
	public static File factionDirectory = new File("plugins/OwnFactions");
	public static File factionFile = new File("plugins/OwnFactions/factionData.yml");
	public static File playerFile = new File("plugins/OwnFactions/playerData.yml");
	
	public static FileConfiguration factionConfig = new YamlConfiguration();
	public static FileConfiguration playerConfig = new YamlConfiguration();
	
	ArrayList<records.factionRec> factionsList = records.factionlist;
	ArrayList<records.playerRec> playerList = records.playerlist;
	
	public static void directoryIfNotExist(File file) {
		if (!file.exists()) {
			file.mkdir();
		}
	}
	
	public static void fileIfNotExist(File file) {
		if (!file.exists()) {
			try {
				file.createNewFile();
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
}
