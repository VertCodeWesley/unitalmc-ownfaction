package me.DGCWesley.Faction.records;

import java.util.ArrayList;

import org.bukkit.Location;

public class records {
	
	public static class factionRec {
		public String name;
		public String owner;
		public String mod;
		public Location fhome;
		public ArrayList<factionMemberRec> members = new ArrayList<factionMemberRec>();
	}
	
	public static class factionMemberRec {
		public playerRec player;
		public String rank;
	}
	
	public static class playerRec {
		public String name;
		public String uuid;
		public factionRec faction;
	}
	
	public static ArrayList<factionRec> factionlist = new ArrayList<factionRec>();
	public static ArrayList<playerRec> playerlist = new ArrayList<playerRec>();
	
}
